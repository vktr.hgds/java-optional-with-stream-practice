package com.hegedusviktor;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    private static Random random = new Random();

    public static void main(String[] args) {
        Optional<String> str = Optional.ofNullable(null);
        Optional<String> str2 = Optional.ofNullable("notnull");
        System.out.println(str.isPresent() ? str.get() : "null str");
        System.out.println(str2.isPresent() ? str2.get() : "null str2");

        List<Integer> list = generateNumbers(10);
        list.stream()
                .map(Optional::ofNullable)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(System.out::println);
    }

    private static List<Integer> generateNumbers(int n) {
        List<Integer> list = IntStream
                .range(0, n)
                .map(i -> random.nextInt(100))
                .boxed()
                .collect(Collectors.toList());

        return list.stream()
                .map(i -> i % 2 == 0 ? null : i)
                .collect(Collectors.toList());
    }
}
